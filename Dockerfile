FROM ruby:2.5

WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
ENV BUNDLE_FROZEN=true
RUN bundle install

COPY . .

ENV PORT 8080
EXPOSE $PORT

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "8080"]
